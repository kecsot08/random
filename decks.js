import {
    ADD_DECK,
    ADD_DECK_FAILURE,
    ADD_DECK_SUCCESS,
    DELETE_DECK,
    DELETE_DECK_FAILURE,
    DELETE_DECK_SUCCESS,
    LOAD_DECKS,
    LOAD_DECKS_FAILURE,
    LOAD_DECKS_SUCCESS,
    UPDATE_DECK,
    UPDATE_DECK_FAILURE,
    UPDATE_DECK_SUCCESS
} from "../actions/decks";

const initialState = {
    deckList: [],
    isDeckListLoading: false,
    isErrorHappened: false,

    // Add
    isDeckAddSuccess: false,
    isDeckAddLoading: false,
    deckAddError: null,

    // Update
    isDeckUpdateSuccess: false,
    isDeckUpdateLoading: false,
    deckUpdateError: null,

    // Delete
    isDeckDeleteSuccess: false,
    isDeckDeleteLoading: false,
    deckDeleteError: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_DECKS:
            return {
                isDeckListLoading: true,
                deckList: [],
                isErrorHappened: false
            };
        case LOAD_DECKS_SUCCESS:
            return {
                isDeckListLoading: false,
                deckList: action.payload,
                isErrorHappened: false
            };
        case LOAD_DECKS_FAILURE:
            return {
                isDeckListLoading: false,
                deckList: [],
                isErrorHappened: true
            };
        case ADD_DECK:
            return {
                ...state,
                isDeckAddSuccess: false,
                isDeckAddLoading: true,
                deckAddError: null
            }
        case ADD_DECK_SUCCESS: {
            return {
                ...state,
                deckList: [...state.deckList, action.payload.deck],
                isDeckAddSuccess: true,
                isDeckAddLoading: false,
                deckAddError: null
            }
        }
        case ADD_DECK_FAILURE:
            return {
                ...state,
                isDeckAddSuccess: false,
                isDeckAddLoading: false,
                deckAddError: action.payload.error
            }
        case UPDATE_DECK:
            return {
                ...state,
                isDeckUpdateSuccess: false,
                isDeckUpdateLoading: true,
                deckAddError: null
            }
        case UPDATE_DECK_SUCCESS: {
            return {
                ...state,
                deckList: state.deckList.map(
                    (item) => action.payload.deck.id === item.id ? action.payload.deck : item
                ),
                isDeckUpdateSuccess: true,
                isDeckUpdateLoading: false,
                deckAddError: null
            }
        }
        case UPDATE_DECK_FAILURE:
            return {
                ...state,
                isDeckUpdateSuccess: false,
                isDeckUpdateLoading: false,
                deckUpdateError: action.payload.error
            }

        case DELETE_DECK:
            return {
                ...state,
                isDeckDeleteSuccess: false,
                isDeckDeleteLoading: true,
                deckUpdateError: null
            }
        case DELETE_DECK_SUCCESS:
            return {
                ...state,
                deckList: state.deckList.filter(item => item.id !== action.payload.deckId),
                isDeckDeleteSuccess: true,
                isDeckDeleteLoading: false,
                deckUpdateError: null
            }
        case DELETE_DECK_FAILURE:
            return {
                ...state,
                isDeckDeleteSuccess: false,
                isDeckDeleteLoading: false,
                deckUpdateError: action.payload.error
            }
        default:
            return state;
    }
}

export default reducer;