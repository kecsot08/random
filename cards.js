import {
    ADD_CARD,
    ADD_CARD_FAILURE,
    ADD_CARD_SUCCESS,
    DELETE_CARD,
    DELETE_CARD_FAILURE,
    DELETE_CARD_SUCCESS,
    LOAD_CARD_BY_ID,
    LOAD_CARD_BY_ID_FAILURE,
    LOAD_CARD_BY_ID_SUCCESS,
    LOAD_CARDS_OF_DECK,
    LOAD_CARDS_OF_DECK_SUCCESS,
    UPDATE_CARD,
    UPDATE_CARD_FAILURE,
    UPDATE_CARD_SUCCESS
} from "../actions/cards";

const initialState = {
    // Load a list
    cardList: [],
    cardListDeckId: null,
    isCardListLoading: false,
    cardListError: null,

    // Add
    isCardAddSuccess: false,
    isCardAddLoading: false,
    cardAddError: null,

    // Update
    isCardUpdateSuccess: false,
    isCardUpdateLoading: false,
    cardUpdateError: null,

    // Delete
    isCardDeleteSuccess: false,
    isCardDeleteLoading: false,
    cardDeleteError: null,

    // Load one item
    card: null,
    cardId: null,
    isCardLoading: false,
    cardError: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOAD_CARDS_OF_DECK:
            return {
                ...state,
                isCardListLoading: true,
                cardList: [],
                cardListDeckId: null,
                cardListError: null
            }
        case LOAD_CARDS_OF_DECK_SUCCESS:
            return {
                ...state,
                isCardListLoading: false,
                cardList: action.payload.cardList,
                cardListDeckId: action.payload.deckId,
                cardListError: null
            }

        case ADD_CARD:
            return {
                ...state,
                isCardAddSuccess: false,
                isCardAddLoading: true,
                cardAddError: null
            }
        case ADD_CARD_SUCCESS: {
            return {
                ...state,
                cardList: [...state.cardList, action.payload.card],
                isCardAddSuccess: true,
                isCardAddLoading: false,
                cardAddError: null
            }
        }
        case ADD_CARD_FAILURE:
            return {
                ...state,
                isCardAddSuccess: false,
                isCardAddLoading: false,
                cardAddError: action.payload.error
            }


        case LOAD_CARD_BY_ID:
            return {
                ...state,
                card: null,
                cardId: null,
                isCardLoading: true,
                cardError: null,
            }
        case LOAD_CARD_BY_ID_SUCCESS:
            return {
                ...state,
                card: action.payload.card,
                cardId: action.payload.cardId,
                isCardLoading: false,
                cardError: null,
            }
        case LOAD_CARD_BY_ID_FAILURE:
            return {
                ...state,
                card: null,
                cardId: null,
                isCardLoading: false,
                cardError: action.payload.error,
            }

        case UPDATE_CARD:
            return {
                ...state,
                isCardUpdateSuccess: false,
                isCardUpdateLoading: true,
                cardUpdateError: null
            }
        case UPDATE_CARD_SUCCESS: {
            return {
                ...state,
                cardList: state.cardList.map(
                    (item) => action.payload.card.id === item.id ? action.payload.card : item
                ),
                isCardUpdateSuccess: true,
                isCardUpdateLoading: false,
                cardUpdateError: null
            }
        }
        case UPDATE_CARD_FAILURE:
            return {
                ...state,
                isCardUpdateSuccess: false,
                isCardUpdateLoading: false,
                cardUpdateError: action.payload.error
            }

        case DELETE_CARD:
            return {
                ...state,
                isCardDeleteSuccess: false,
                isCardDeleteLoading: true,
                cardDeleteError: null
            }
        case DELETE_CARD_SUCCESS:
            return {
                ...state,
                cardList: state.cardList.filter(item => item.id !== action.payload.cardId),
                isCardDeleteSuccess: true,
                isCardDeleteLoading: false,
                cardDeleteError: null
            }
        case DELETE_CARD_FAILURE:
            return {
                ...state,
                isCardDeleteSuccess: false,
                isCardDeleteLoading: false,
                cardDeleteError: action.payload.error
            }
        default:
            return state;
    }
}

export default reducer;